package com.lizardoreyes.practicaandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.practicaandroid.databinding.ActivityInfoBinding

class InfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = getString(R.string.title_info)

        binding.btnCloseInfo.setOnClickListener {
            finish()
        }
    }
}