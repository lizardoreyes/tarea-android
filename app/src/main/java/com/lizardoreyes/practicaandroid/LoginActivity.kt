package com.lizardoreyes.practicaandroid

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lizardoreyes.practicaandroid.databinding.ActivityLoginBinding
import com.lizardoreyes.practicaandroid.utils.AlertHelper.Companion.showAlertDialog

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {

            // DEFAULT EMAIL   :  lizardo@email.com
            // DEFAULT PASSWORD:  123456

            val email = binding.edtEmail.text.toString()
            val password = binding.edtPassword.text.toString()

            if (validateLogin(email, password)) {
                val bundle = Bundle().apply {
                    putString("email", email)
                    putString("password", password)
                }

                val intent = Intent(this, MainActivity::class.java).apply {
                    putExtras(bundle)
                }

                startActivity(intent)
            } else {
                showAlertDialog(
                    this,
                    getString(R.string.wrong_credentials),
                    getString(R.string.wrong_credentials_description)
                )
            }
        }
    }

    private fun validateLogin(username: String, password: String): Boolean {
        if (username.isNotEmpty() && password.isNotEmpty()
            && username == "lizardo@email.com" && password == "123456") {
            return true
        }
        return false
    }
}