package com.lizardoreyes.practicaandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.lizardoreyes.practicaandroid.adapters.MascotAdapter
import com.lizardoreyes.practicaandroid.databinding.ActivityMascotBinding
import com.lizardoreyes.practicaandroid.models.Mascot
import com.lizardoreyes.practicaandroid.utils.AlertHelper.Companion.showAlertDialog

class MascotActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMascotBinding
    private var mascots: MutableList<Mascot> = mutableListOf()
    private lateinit var adapter: MascotAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMascotBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = getString(R.string.title_mascots)

        adapter = MascotAdapter { _, mascot ->
            showAlertDialog(
                this,
                getString(R.string.alert_title_mascot, mascot.name),
                getString(R.string.alert_description_mascot, mascot.name, mascot.race)
            )
        }

        binding.rvMascot.adapter = adapter
        binding.rvMascot.layoutManager = LinearLayoutManager(this)

        Handler().postDelayed({
            adapter.addAll(loadData())
            binding.progress.visibility = View.GONE
        }, 3000)

    }

    private fun loadData(): MutableList<Mascot> {
        mascots.add(Mascot("Rocky", "Labrador"))
        mascots.add(Mascot("Boddy", "Pastor Aleman"))
        mascots.add(Mascot("Beethoven", "San Bernardo"))
        mascots.add(Mascot("Hachiko", "Akita Inu"))
        mascots.add(Mascot("Laika", "Rusa"))
        mascots.add(Mascot("Pongo", "Dálmata"))
        mascots.add(Mascot("Scooby Doo", "Scooby Doo"))
        mascots.add(Mascot("Goofy", "Disney"))
        mascots.add(Mascot("Niebla", "Heidi"))
        return mascots
    }
}