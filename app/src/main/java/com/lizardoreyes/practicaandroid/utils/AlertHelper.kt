package com.lizardoreyes.practicaandroid.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.lizardoreyes.practicaandroid.R

class AlertHelper {

     companion object {

         fun showAlertDialog(context: Context, title: String, message: String) {
             val builder: AlertDialog.Builder = AlertDialog.Builder(context)
             builder.setTitle(title)
             builder.setMessage(message)
             builder.setPositiveButton(context.getString(R.string.ok), null)
             val alertDialog: AlertDialog = builder.create()
             alertDialog.show()
         }
     }

}