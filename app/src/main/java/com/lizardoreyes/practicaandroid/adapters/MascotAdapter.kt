package com.lizardoreyes.practicaandroid.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lizardoreyes.practicaandroid.databinding.ItemMascotBinding
import com.lizardoreyes.practicaandroid.models.Mascot

class MascotAdapter (
    private var mascots: MutableList<Mascot> = mutableListOf(),
    private val itemCallbackMascot : (position: Int, item: Mascot)-> Unit
): RecyclerView.Adapter<MascotAdapter.MascotAdapterViewHolder>() {
    private lateinit var binding: ItemMascotBinding

    inner class MascotAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val nameMascot: TextView = binding.tvNameMascot
        private val raceMascot: TextView = binding.tvRaceMascot

        fun bind(position: Int, mascot: Mascot, itemCallbackMascot: (position: Int, item: Mascot) -> Unit) {
            nameMascot.text = mascot.name
            raceMascot.text = mascot.race

            itemView.setOnClickListener {
                itemCallbackMascot(position, mascot)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotAdapterViewHolder {
        binding = ItemMascotBinding.inflate(LayoutInflater.from(parent.context))
        return MascotAdapterViewHolder(binding.root)

    }

    override fun onBindViewHolder(holder: MascotAdapterViewHolder, position: Int) {
        val mascot = mascots[position]
        holder.bind(position, mascot, itemCallbackMascot)
    }

    override fun getItemCount(): Int = this.mascots.size

    fun addAll(mascots: MutableList<Mascot>) {
        this.mascots = mascots
        notifyDataSetChanged()
    }
}