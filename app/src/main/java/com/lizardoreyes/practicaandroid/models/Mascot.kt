package com.lizardoreyes.practicaandroid.models

data class Mascot(val name: String, val race: String)
