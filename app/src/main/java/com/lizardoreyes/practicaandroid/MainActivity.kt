package com.lizardoreyes.practicaandroid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.practicaandroid.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // jledesma2509@gmail.com
        // Asunto : Practica calificado Diplomado 2021 - II

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.title = getString(R.string.title_home)

        val extras = intent.extras
        val email = extras?.getString("email") ?: "No hay email"
        binding.tvEmail.text = getString(R.string.welcome_home, email)

        binding.btnLogout.setOnClickListener {
            finish()
        }

        binding.btnInfo.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java)
            startActivity(intent)
        }
        binding.btnMascot.setOnClickListener {
            val intent = Intent(this, MascotActivity::class.java)
            startActivity(intent)
        }

    }


}